package com.example.loginmvpapp.presenter.login

import com.example.loginmvpapp.presenter.BasePresenterContract

interface LoginPresenterContract: BasePresenterContract {
    fun login(email: String, password: String)
}
