package com.example.loginmvpapp.presenter

import com.example.loginmvpapp.ui.base.BaseViewContract

interface BasePresenterContract {
    fun setView(view : BaseViewContract)

    fun dispose()
}
