package com.example.loginmvpapp.presenter.login

import com.example.loginmvpapp.R
import com.example.loginmvpapp.presenter.BasePresenter
import com.example.loginmvpapp.repository.login.LoginRepository
import com.example.loginmvpapp.ui.base.BaseViewContract
import com.example.loginmvpapp.ui.login.LoginViewContract
import javax.inject.Inject

class LoginPresenter @Inject constructor(val repository: LoginRepository) : BasePresenter(),
    LoginPresenterContract {

    private lateinit var view: LoginViewContract

    override fun setView(view: BaseViewContract) {
        super.setView(view)
        this.view = view as LoginViewContract
    }

    override fun login(email: String, password: String) {
        dispose()
        disposable = repository.login(email, password)
            .subscribe({
                dispose()
                view.showToast("Login Success")
            }, {
                dispose()
                view.showToast("Login Failed")
            })
    }
}
