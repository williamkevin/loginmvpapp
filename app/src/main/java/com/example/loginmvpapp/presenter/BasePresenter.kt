package com.example.loginmvpapp.presenter

import com.example.loginmvpapp.ui.base.BaseViewContract
import io.reactivex.disposables.Disposable

open class BasePresenter : BasePresenterContract {

    private lateinit var view: BaseViewContract

    internal var disposable: Disposable? = null

    override fun setView(view: BaseViewContract) {
        this.view = view
    }

    override fun dispose() {
        disposable?.let {
            if (!disposable!!.isDisposed) {
                disposable!!.dispose()
            }
            disposable = null
        }
    }
}
