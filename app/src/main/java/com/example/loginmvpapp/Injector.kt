package com.example.loginmvpapp

import android.content.Context
import com.example.loginmvpapp.di.DaggerEmailMvpAppComponent
import com.example.loginmvpapp.di.EmailMvpAppGraph
import com.example.loginmvpapp.di.EmailMvpAppModule

object Injector {
    fun obtain(context: Context): EmailMvpAppGraph {
        return EmailMvpApp.get(context).getInjector()
    }

    internal fun create(app: EmailMvpApp): EmailMvpAppGraph {
        return DaggerEmailMvpAppComponent.builder().emailMvpAppModule(EmailMvpAppModule(app)).build()
    }
}
