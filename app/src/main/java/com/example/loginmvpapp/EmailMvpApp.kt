package com.example.loginmvpapp

import android.app.Application
import android.content.Context
import com.example.loginmvpapp.di.EmailMvpAppGraph

class EmailMvpApp: Application() {
    private lateinit var objectGraph: EmailMvpAppGraph

    override fun onCreate() {
        super.onCreate()

        objectGraph = Injector.create(this)
        objectGraph.inject(this)

    }

    fun getInjector(): EmailMvpAppGraph {
        return objectGraph
    }

    companion object {
        fun get(context: Context): EmailMvpApp {
            return context.applicationContext as EmailMvpApp
        }
    }
}
