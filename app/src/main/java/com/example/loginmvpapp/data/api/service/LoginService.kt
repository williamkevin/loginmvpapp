package com.example.loginmvpapp.data.api.service

import com.example.loginmvpapp.data.model.request.User
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.POST

interface LoginService {

    @POST("signin")
    fun login(@Body user: User): Single<User>

}
