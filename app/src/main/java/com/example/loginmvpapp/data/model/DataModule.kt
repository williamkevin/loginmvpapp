package com.example.loginmvpapp.data.model

import com.example.loginmvpapp.data.api.EmailMvpApiModule
import com.example.loginmvpapp.repository.login.LoginDataSource
import com.example.loginmvpapp.repository.login.LoginRemoteDataSource
import com.example.loginmvpapp.repository.login.LoginRepository
import com.example.loginmvpapp.scheduler.BaseSchedulerProvider
import com.example.loginmvpapp.scheduler.SchedulerProvider
import com.example.loginmvpapp.data.api.service.LoginService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes=[EmailMvpApiModule::class])
class DataModule {

    @Provides
    @Singleton
    internal fun provideScheduler(): BaseSchedulerProvider = SchedulerProvider.getInstance()

    @Provides
    @Singleton
    internal fun provideGenreRemoteDataSource(loginService: LoginService,
                                              schedulerProvider: BaseSchedulerProvider):
            LoginDataSource = LoginRemoteDataSource(loginService, schedulerProvider)

    @Provides
    @Singleton
    internal fun provideGenreRepository(remoteDataSource: LoginRemoteDataSource):
            LoginRepository = LoginRepository(remoteDataSource)

}