package com.example.loginmvpapp.repository.login

import com.example.loginmvpapp.data.model.request.User
import com.example.loginmvpapp.scheduler.BaseSchedulerProvider
import com.example.loginmvpapp.data.api.service.LoginService
import io.reactivex.Single
import javax.inject.Inject

class LoginRemoteDataSource @Inject
constructor(private val loginService: LoginService,
            private val schedulerProvider: BaseSchedulerProvider) : LoginDataSource {

    override fun login(email: String, password: String): Single<User> {
        val request = User(email, password)
        return loginService.login(request)
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
    }

}
