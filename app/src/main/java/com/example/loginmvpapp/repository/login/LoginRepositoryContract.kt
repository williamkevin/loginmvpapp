package com.example.loginmvpapp.repository.login

import com.example.loginmvpapp.data.model.request.User
import io.reactivex.Single

interface LoginRepositoryContract {
    fun login(email: String, password: String): Single<User>
}
