package com.example.loginmvpapp.repository.login

import com.example.loginmvpapp.data.model.request.User
import io.reactivex.Single

interface LoginDataSource {
    fun login(email: String, password: String): Single<User>
}
