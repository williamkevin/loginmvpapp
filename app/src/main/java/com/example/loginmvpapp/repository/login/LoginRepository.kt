package com.example.loginmvpapp.repository.login

import com.example.loginmvpapp.data.model.request.User
import io.reactivex.Single

class LoginRepository (private val remoteDataSource: LoginRemoteDataSource)
    :LoginRepositoryContract {

    override fun login(email: String, password: String): Single<User> {
        return remoteDataSource.login(email, password)
    }
}
