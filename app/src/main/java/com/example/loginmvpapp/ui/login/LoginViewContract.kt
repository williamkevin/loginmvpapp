package com.example.loginmvpapp.ui.login

import com.example.loginmvpapp.ui.base.BaseViewContract

interface LoginViewContract: BaseViewContract {
    fun login()
}
