package com.example.loginmvpapp.ui.login

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.Toast
import com.example.loginmvpapp.Injector
import com.example.loginmvpapp.R
import com.example.loginmvpapp.presenter.login.LoginPresenter
import com.example.loginmvpapp.ui.base.BaseActivity
import com.example.loginmvpapp.utils.Utils
import kotlinx.android.synthetic.main.login_activity.*
import javax.inject.Inject

class LoginActivity : BaseActivity(), LoginViewContract {

    @Inject lateinit var presenter: LoginPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.login_activity)
        Injector.obtain(this).inject(this)

        presenter.setView(this)

        validationEmail()

        btn_login.setOnClickListener{ login() }
    }

    override fun login() {
        val email = edt_email.text.toString()
        val password = edt_password.text.toString()

        if (email.isEmpty() || password.isEmpty()) {
            showToast(getString(R.string.error_message_empty))
        }

        presenter.login(email, password)
    }

    private fun validationEmail() {
        edt_email.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                when {
                    s.toString().isEmpty() -> {
                        edt_email.error = getString(R.string.error_message_empty)
                    }
                    Utils.isEmailValid(s.toString().trim()) -> {
                        edt_email.error = null
                    }
                    else -> {
                        edt_email.error = getString(R.string.error_invalid_email)
                    }
                }
            }
        })
    }

    override fun showToast(message: String) {
        Utils.showToastFollowed(this, message)
    }
}
