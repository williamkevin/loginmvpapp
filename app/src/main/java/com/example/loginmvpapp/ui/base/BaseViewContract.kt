package com.example.loginmvpapp.ui.base

interface BaseViewContract {
    fun showToast(message: String)
}
