package com.example.loginmvpapp.ui.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.loginmvpapp.Injector

abstract class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        Injector.obtain(this).inject(this)
        super.onCreate(savedInstanceState)
    }

}
