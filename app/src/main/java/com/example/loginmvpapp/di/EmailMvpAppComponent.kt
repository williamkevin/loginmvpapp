package com.example.loginmvpapp.di

import com.example.loginmvpapp.data.model.DataModule
import dagger.Component
import javax.inject.Singleton

@Component(
    modules = [
        EmailMvpAppModule::class,
        DataModule::class]
)

@Singleton
interface EmailMvpAppComponent : EmailMvpAppGraph
