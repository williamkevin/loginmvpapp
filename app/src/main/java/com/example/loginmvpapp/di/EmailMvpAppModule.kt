package com.example.loginmvpapp.di

import android.app.Application
import android.content.ContentResolver
import android.content.Context
import com.example.loginmvpapp.EmailMvpApp
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class EmailMvpAppModule (val application : EmailMvpApp) {

    @Provides
    @Singleton
    fun provideApplication(): Application = application

    @Provides
    @Singleton
    fun provideContext(): Context = application

    @Provides
    @Singleton
    fun provideContentResolver(): ContentResolver = application.contentResolver
}
