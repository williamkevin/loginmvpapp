package com.example.loginmvpapp.di

import com.example.loginmvpapp.EmailMvpApp
import com.example.loginmvpapp.ui.base.BaseActivity
import com.example.loginmvpapp.ui.login.LoginActivity

interface EmailMvpAppGraph {

    fun inject(app: EmailMvpApp)

    fun inject(baseActivity: BaseActivity)

    fun inject(loginActivity: LoginActivity)

}
