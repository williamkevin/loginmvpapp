package com.example.loginmvpapp.utils

import android.content.Context
import android.widget.Toast
import java.util.regex.Pattern

class Utils {
    companion object {

        fun toastAboveNavBar(context: Context, message: String): Toast {
            return Toast.makeText(context,
                message, Toast.LENGTH_SHORT)
        }

        fun showToastFollowed(context: Context, message: String) {
            toastAboveNavBar(context, message).show()
        }

        fun isEmailValid(email: String): Boolean {
            val regExpn = ("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                    + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                    + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                    + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$")
            val inputStr: CharSequence = email
            val pattern =
                Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE)
            val matcher = pattern.matcher(inputStr)
            return matcher.matches()
        }
    }
}
